import './App.css';
import Photo from './components/photo'
function App() {
  return (
    <div className="App">
      <div className="fetched-wrapper">
        <Photo title = "Cairn" url='/cairn/images/random'/>
        <Photo title = "Pitbull" url='/pitbull/images/random'/>
        <Photo title = "Havanese" url='/havanese/images/random'/>
        <Photo title = "Boxer" url='/boxer/images/random'/>
        <Photo title = "Beagle" url='/beagle/images/random'/>
      </div>
    </div>
  );
}

export default App;
